package excuses

import (
	"strings"
	"testing"
)

func TestExcuses(t *testing.T) {
	excuse := NewExcuse("Test")

	if !strings.Contains(excuse, "Test cannot make it to work today because") {
		t.Errorf("does not contain default output")
	}
}
